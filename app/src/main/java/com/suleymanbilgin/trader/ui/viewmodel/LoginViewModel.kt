package com.suleymanbilgin.trader.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.repository.LoginRepository


class LoginViewModel : ViewModel() {
    var loginLiveData = MutableLiveData<Login>()

    fun login(username: String, password: String): LiveData<Login> {
        loginLiveData  = LoginRepository.login(username, password)
        return loginLiveData
    }

}