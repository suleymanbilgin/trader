package com.suleymanbilgin.trader.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.suleymanbilgin.trader.R
import com.suleymanbilgin.trader.databinding.ActivityMainBinding
import com.suleymanbilgin.trader.model.Item
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class PositionsAdapter(private val activityMainBinding: ActivityMainBinding, private val dataSet: List<Item>) :
    RecyclerView.Adapter<PositionsAdapter.ViewHolder>() {

    var sumGlobal : Double = 0.0

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val row: ConstraintLayout
        val tvSymbol: TextView
        val tvQuantity: TextView
        val tvPrice: TextView
        val tvAmount: TextView

        init {
            // Define click listener for the ViewHolder's View.
            row = view.findViewById(R.id.row)
            tvSymbol = view.findViewById(R.id.tv_symbol)
            tvQuantity = view.findViewById(R.id.tv_quantity)
            tvPrice = view.findViewById(R.id.tv_price)
            tvAmount = view.findViewById(R.id.tv_amount)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_position, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val item: Item = dataSet[position]

        if (position % 2 == 1) {
            viewHolder.row.setBackgroundColor(Color.LTGRAY)
        }

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.tvSymbol.text = item.Symbol
        viewHolder.tvQuantity.text = item.Qty_T2.toInt().toString()
        viewHolder.tvPrice.text = item.LastPx.toString().replace('.', ',')

        val format : NumberFormat = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(5);
        val sum: Double = item.Qty_T2 * item.LastPx
        sumGlobal = sumGlobal + sum
        activityMainBinding.tvSumAmount.text =format.format(sumGlobal)
        viewHolder.tvAmount.text = format.format(sum)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size


}