package com.suleymanbilgin.trader.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.suleymanbilgin.trader.R
import com.suleymanbilgin.trader.databinding.ActivityLoginBinding
import com.suleymanbilgin.trader.helper.PreferencesManager
import com.suleymanbilgin.trader.helper.Utils
import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.repository.LoginRepository
import com.suleymanbilgin.trader.ui.viewmodel.LoginViewModel

class LoginActivity : AppCompatActivity() {

    lateinit var preferencesManager: PreferencesManager
    lateinit var context: Context

    val loginViewModel: LoginViewModel by viewModels()


    private lateinit var binding: ActivityLoginBinding
    private lateinit var loginRepository: LoginRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this@LoginActivity
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        preferencesManager = PreferencesManager(this@LoginActivity)
        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val password = binding.etPassword.text.toString()

            login(username, password)
        }
    }

    private fun login(username: String, password: String) {
        if (username == "" || password == "") {
            Toast.makeText(this@LoginActivity, getString(R.string.control_input), Toast.LENGTH_LONG)
                .show()
            return
        }

        loginViewModel.login(username, password).observe(this@LoginActivity, {
            if (it.Result.State) {
                preferencesManager.loginData = Gson().toJson(it)
                preferencesManager.username = username
                preferencesManager.password = password

                val intent: Intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Utils.showDialog(this@LoginActivity, it.Result.Description)
            }
            loginViewModel.loginLiveData = MutableLiveData<Login>()
        })
    }

}