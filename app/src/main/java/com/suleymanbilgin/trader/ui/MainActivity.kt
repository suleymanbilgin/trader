package com.suleymanbilgin.trader.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.suleymanbilgin.trader.R
import com.suleymanbilgin.trader.databinding.ActivityLoginBinding
import com.suleymanbilgin.trader.databinding.ActivityMainBinding
import com.suleymanbilgin.trader.helper.PreferencesManager
import com.suleymanbilgin.trader.repository.LoginRepository
import com.suleymanbilgin.trader.repository.MainActivityRepository
import com.suleymanbilgin.trader.ui.adapter.PositionsAdapter
import com.suleymanbilgin.trader.ui.viewmodel.LoginViewModel
import com.suleymanbilgin.trader.ui.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    companion object {
        val TAG = LoginActivity::class.java.simpleName
    }

    lateinit var preferencesManager: PreferencesManager
    lateinit var context: Context
    val mainViewModel: MainActivityViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this@MainActivity
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        preferencesManager = PreferencesManager(context)

        binding.swiperefresh.setOnRefreshListener(this)
        getPositions()
    }

    fun getPositions() {
        status(R.color.primary, getString(R.string.loading))
        mainViewModel.getPositions(context).observe(this@MainActivity, {
            if (it.result.State) {
                binding.rvPositions.layoutManager = LinearLayoutManager(context)
                if (it.itemList.size > 0) {
                    val positionsAdapter = PositionsAdapter(binding, it.itemList)
                    binding.rvPositions.adapter = positionsAdapter
                }
                status(R.color.green, it.result.Description)
            } else {
                status(android.R.color.holo_red_dark, it.result.Description)
            }

            binding.swiperefresh.isRefreshing = false
        })
    }

    fun status(color: Int, text: String) {
        binding.tvAlert.setBackgroundColor(getColor(color))
        binding.tvAlert.text = text
    }

    override fun onRefresh() {
        getPositions()
    }
}