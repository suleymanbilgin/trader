package com.suleymanbilgin.trader.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.suleymanbilgin.trader.helper.PreferencesManager
import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.model.Position
import com.suleymanbilgin.trader.repository.MainActivityRepository

class MainActivityViewModel : ViewModel() {

    var poisitionsData = MutableLiveData<Position>()

    fun getPositions(context: Context): LiveData<Position> {
        val preferencesManager = PreferencesManager(context)
        val loginData: Login = Gson().fromJson(preferencesManager.loginData, Login::class.java)
        poisitionsData = MainActivityRepository.getPositions(
            preferencesManager.username,
            preferencesManager.password,
            loginData.DefaultAccount.toInt()
        )
        return poisitionsData
    }

}