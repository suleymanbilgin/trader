package com.suleymanbilgin.trader.model

class AccountRights(
    val Code: String,
    val Key: String,
    val Value: String,
    val Timestamp: Int,
    val DataType: Int
)