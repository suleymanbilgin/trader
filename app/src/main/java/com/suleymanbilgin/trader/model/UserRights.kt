package com.suleymanbilgin.trader.model

class UserRights(
    val Code: String,
    val Key: String,
    val Value: String,
    val Timestamp: Int,
    val DataType: Int
)