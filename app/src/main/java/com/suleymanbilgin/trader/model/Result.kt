package com.suleymanbilgin.trader.model

class Result(
    val State: Boolean,
    val Code: Int,
    val Description: String,
    val SessionKey: String,
    val Duration: Int,
    val MsgType: String,
    val Timestamp: String,
    val ClOrdID: String,
    val Reserved: String,
    val V: String,
)
