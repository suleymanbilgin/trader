package com.suleymanbilgin.trader.model

class AccountItems(
    val AccountID: String,
    val ExchangeAccountID: ExchangeAccountID,
    val AccountRights: List<AccountRights>,
    val TransactionTypeRights: List<TransactionTypeRights>,
    val Reserved: String,
    val AccountIDExtended: String
)