package com.suleymanbilgin.trader.model

class Rights(
    val Side: String,
    val L: List<String>
)