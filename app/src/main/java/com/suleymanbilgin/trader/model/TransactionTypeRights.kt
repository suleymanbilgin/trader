package com.suleymanbilgin.trader.model

class TransactionTypeRights(
    val ExchangeID: Int,
    val Rights: List<Rights>
)