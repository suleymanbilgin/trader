package com.suleymanbilgin.trader.model

import com.google.gson.annotations.SerializedName

class Position(
    @SerializedName("Result") val result: Result,
    @SerializedName("Header") val header: List<String>,
    @SerializedName("Item") val itemList: List<Item>
)