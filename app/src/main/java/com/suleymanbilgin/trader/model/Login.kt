package com.suleymanbilgin.trader.model

class Login(
    val Result: Result,
    val AccountList: List<String>,
    val DefaultAccount: String,
    val CustomerID: String,
    val UserRights: List<UserRights>,
    val AccountItems: List<AccountItems>,
    val MarketDataToken: String,
    val CustomerName: String,
    val ExCode: Int,
    val AccountListEx: List<Void>,
    val LicenceList: List<Void>,
    val DevicePairToken: String,
    val Statistics: Void,
    val MatriksID: String
)