package com.suleymanbilgin.trader.api

import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.model.Position
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET(".")
    fun login(
        @Query("MsgType") MsgTypeA : String = "A",
        @Query("CustomerNo") CustomerNo : Int = 0,
        @Query("Username") Username : String,
        @Query("Password") Password : String,
        @Query("AccountID") AccountID : Int = 0,
        @Query("ExchangeID") ExchangeID : Int = 4,
        @Query("OutputType") OutputType : Int = 2,
    ): Call<Login>


    @GET(".")
    fun getPositions(
        @Query("MsgType") MsgTypeA : String = "AN",
        @Query("CustomerNo") CustomerNo : Int = 0,
        @Query("Username") Username : String,
        @Query("Password") Password : String,
        @Query("AccountID") AccountID : Int,
        @Query("ExchangeID") ExchangeID : Int = 4,
        @Query("OutputType") OutputType : Int = 2,
    ): Call<Position>






}