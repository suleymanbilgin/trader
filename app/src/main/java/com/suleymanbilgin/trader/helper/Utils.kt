package com.suleymanbilgin.trader.helper

import android.app.Dialog
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.suleymanbilgin.trader.R

class Utils {

    companion object {
        fun showDialog(activity: AppCompatActivity, msg: String) {
            val dialog: Dialog = Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_error);

            val text = dialog.findViewById<TextView>(R.id.tv_error);
            text.setText(msg);

            dialog.show();
        }
    }
}