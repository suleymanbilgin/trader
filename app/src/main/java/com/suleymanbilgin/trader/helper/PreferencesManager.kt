package com.suleymanbilgin.trader.helper

import android.content.Context
import com.github.rtoshiro.secure.SecureSharedPreferences

class PreferencesManager (context: Context) {

    companion object {
        private const val LOGIN_DATA = "login_data"
        private const val USERNAME = "username"
        private const val PASSWORD = "password"
    }

    private val sharedPreferences = SecureSharedPreferences(context)
    private val editor = sharedPreferences.edit(true)

    var loginData: String
        get() = sharedPreferences.getString(LOGIN_DATA, "")
        set(value) {
            editor.putString(LOGIN_DATA, value)
        }

    var username: String
        get() = sharedPreferences.getString(USERNAME, "")
        set(value) {
            editor.putString(USERNAME, value)
        }

    var password: String
        get() = sharedPreferences.getString(PASSWORD, "")
        set(value) {
            editor.putString(PASSWORD, value)
        }

    fun logout() {
        editor.clear()
    }
}