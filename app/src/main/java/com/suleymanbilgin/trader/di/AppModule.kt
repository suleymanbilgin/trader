package com.suleymanbilgin.trader.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.suleymanbilgin.trader.helper.PreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(
        private val application: Application
) {
    @Provides
    @Singleton
    fun provideApp(): Application = application

    @Provides
    @Singleton
    fun provideContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providePreferencesManager(): PreferencesManager = PreferencesManager(application.applicationContext)

}
