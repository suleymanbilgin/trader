package com.suleymanbilgin.trader.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.suleymanbilgin.trader.api.ApiInterface
import com.suleymanbilgin.trader.api.ServiceBuilder
import com.suleymanbilgin.trader.helper.PreferencesManager
import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.model.Result
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


object LoginRepository {

    val loginSetterGetter = MutableLiveData<Login>()

    fun login(username: String, password: String): MutableLiveData<Login> {

        val call = ServiceBuilder.apiInterface.login(Username = username, Password = password)

        call.enqueue(object: Callback<Login> {
            override fun onFailure(call: Call<Login>, t: Throwable) {
                // TODO("Not yet implemented")
                loginSetterGetter.value = null
            }

            override fun onResponse(
                call: Call<Login>,
                response: Response<Login>
            ) {
                val data = response.body()
                loginSetterGetter.value = data
            }
        })

        return loginSetterGetter
    }


}