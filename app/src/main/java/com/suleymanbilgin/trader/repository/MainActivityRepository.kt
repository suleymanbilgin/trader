package com.suleymanbilgin.trader.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.suleymanbilgin.trader.api.ServiceBuilder
import com.suleymanbilgin.trader.model.Login
import com.suleymanbilgin.trader.model.Position
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MainActivityRepository {

    val positionsSetterGetter = MutableLiveData<Position>()

    fun getPositions(username: String, password: String, accountId: Int): MutableLiveData<Position> {

        val call = ServiceBuilder.apiInterface.getPositions(Username = username, Password = password, AccountID = accountId)

        call.enqueue(object : Callback<Position> {
            override fun onFailure(call: Call<Position>, t: Throwable) {
                // TODO("Not yet implemented")
                positionsSetterGetter.value = null
            }

            override fun onResponse(
                call: Call<Position>,
                response: Response<Position>
            ) {
                val data = response.body()
                positionsSetterGetter.value = data
            }
        })

        return positionsSetterGetter
    }

}

